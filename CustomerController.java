package com.crm.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.crm.springboot.model.Customer;
import com.crm.springboot.service.CustomerService;

@Controller
public class CustomerController {

    @Autowired
    private CustomerService customerService;
    
    // display list of Customers
    @GetMapping("/")
    public String viewHomePage(Model model) {
        model.addAttribute("listCustomers", customerService.getAllCustomers());
        return "index";
    }
    
    @GetMapping("/showNewCustomerForm")
    public String showNewCustomerForm(Model model) {
    	// create model attribute to bind form data
    	Customer customer = new Customer();
        model.addAttribute("customer", customer);
        return "new_customer";
    }

    @PostMapping("/saveCustomer")
    public String saveCustomer(@ModelAttribute("customer") Customer customer) {
        // save Customer to database
    	customerService.saveCustomer(customer);
        return "redirect:/";
    }

    @GetMapping("/showFormForUpdate/{id}")
    public String showFormForUpdate(@PathVariable(value = "id") long id, Model model) {

        // get Customer from the service
    	Customer customer = customerService.getCustomerById(id);

        // set Customer as a model attribute to pre-populate the form
        model.addAttribute("customer", customer);
        return "update_customer";
    }

    @GetMapping("/deleteCustomer/{id}")
    public String deleteCustomer(@PathVariable(value = "id") long id) {

        // call delete Customer method 
        this.customerService.deleteCustomerById(id);
        return "redirect:/";
    }
}