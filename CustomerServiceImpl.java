package com.crm.springboot.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crm.springboot.model.Customer;
import com.crm.springboot.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

	@Override
	public List<Customer> getAllCustomers() {
		// TODO Auto-generated method stub
		return customerRepository.findAll();
	}

	@Override
	public void saveCustomer(Customer customer) {
		// TODO Auto-generated method stub
		this.customerRepository.save(customer);
		
	}

	@Override
	public Customer getCustomerById(long id) {
		// TODO Auto-generated method stub
		 Optional < Customer > optional = customerRepository.findById(id);
		 Customer customer = null;
	        if (optional.isPresent()) {
	        	customer = optional.get();
	        } else {
	            throw new RuntimeException(" Customer not found for id :: " + id);
	        }
	        return customer;
	}

	@Override
	public void deleteCustomerById(long id) {
		// TODO Auto-generated method stub
		this.customerRepository.deleteById(id);
		
	}
}